My collection of container images
=================================

== Global variables

[options="header",frame=all,grid="cols",cols="<.<1%m,<.<50,<.<10,<.<100"]
|============================================
| Name      | Syntax | Examples | Description
| IMAGES
| Separated `all` or any of the `dirname`, `:` for subselection
| `dir,dir2:level1,dir3:level1:level2` +
      `dir1 dir2` +
      `all`
| Fine-grained control of what get's built.

|============================================


include::build/README.asciiDoc[]

include::lang/README.asciiDoc[]

=== notes
* Golang does OCI_BUILD_VOLUMES: "$CI_PROJECT_DIR/base/include/var/lib/RUN:/RUN:ro,z"
but that doesnt exist anymore