ARG FROM_TAG=latest
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Packer with QEMU" \
  org.opencontainers.image.description="Packer and QEMU for virtual image building needs"

COPY --from=scriptsrc include/install/usr/libexec/RUN.d/hashicorp.sh /usr/libexec/RUN.d/

RUN source /usr/libexec/RUN.sh; pkg_run \
	dosfstools genisoimage git-core qemu-img qemu-kvm qemu-system-aarch64 qemu-system-sparc unzip xorriso

ENV \
	CHECKPOINT_DISABLE=1 \
  PACKER_CACHE_DIR=/var/packer/cache \
  PACKER_CONFIG=/etc/packer.d/config \
  PACKER_CONFIG_DIR=/etc/packer.d \
	PACKER_PLUGIN_PATH=/var/packer/plugins

ARG PACKER_VERSION=latest
RUN source /usr/libexec/RUN.sh; hashi_install packer "$PACKER_VERSION"

ENV \
  TMPDIR=/var/packer/tmp

VOLUME /var/packer/cache /var/packer/tmp

EXPOSE 5900:6400

CMD ["/bin/packer"]
