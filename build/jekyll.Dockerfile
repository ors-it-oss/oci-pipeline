ARG FROM_RUBY_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-pipeline/ruby:${FROM_RUBY_TAG}
LABEL \
  org.opencontainers.image.title="Jekyll" \
  org.opencontainers.image.description="Jekyll"

RUN source /usr/libexec/RUN.sh; pkg_run \
	rubygem-http_parser.rb rubygem-eventmachine rubygem-ffi rubygem-sassc

RUN \
  echo Installing packages... ;\
  gem install jekyll
