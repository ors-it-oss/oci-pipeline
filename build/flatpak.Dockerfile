ARG FROM_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="FlatPak builder" \
  org.opencontainers.image.description="For all your FlatPak building needs"

RUN source /usr/libexec/RUN.sh; pkg_run \
	make fuse jq \
  flatpak flatpak-builder flatpak-runtime-config flatpak-selinux flatpak-rpm-macros flatpak-xdg-utils

RUN source /usr/libexec/RUN.sh ;\
	header "Adding repositories..." ;\
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

CMD ["/usr/bin/flatpak-builder"]
