ARG FROM_TAG=latest
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Shell development" \
  org.opencontainers.image.description="Container with useful shell toolz"

RUN source /usr/libexec/RUN.sh; pkg_run \
	diffutils findutils git-core jq make \
	bash bats ShellCheck yamllint

COPY --from=scriptsrc \
  include/install/usr/libexec/RUN.d/github.sh \
  include/install/usr/libexec/RUN.d/lang.sh \
  /usr/libexec/RUN.d/
ARG SHFMT_VERSION=latest
RUN source /usr/libexec/RUN.sh; shfmt_install $SHFMT_VERSION

ENV XDG_CONFIG_HOME=/etc
COPY --chown=0:0 include/yamllint.yml /etc/yamllint/config
