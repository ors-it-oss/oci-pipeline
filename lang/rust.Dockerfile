ARG FROM_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Rust lang container" \
  org.opencontainers.image.description="Container for compiling & linting rust"

RUN source /usr/libexec/RUN.sh; pkg_run \
	bash-completion tmux findutils \
	gcc glibc clang llvm

ENV \
 CARGO_HOME=/var/lib/cargo \
 RUSTUP_HOME=/var/lib/rustup \
 PATH=/var/lib/cargo/bin:${PATH}

RUN source /usr/libexec/RUN.sh ;\
	fetch https://sh.rustup.rs | sh -s --\
		-v -y --profile complete &&\
	/var/lib/cargo/bin/rustup completions bash >/etc/bash_completion.d/rustup

#RUN \
#
#	curl -fLsSv --proto '=https' --tlsv1.2 https://sh.rustup.rs | sh -s -- \
#		-v -y --profile complete &&\
#	/var/lib/cargo/bin/rustup completions bash >/etc/bash_completion.d/rustup
