ARG GOLANG_VERSION
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM docker.io/golang:${GOLANG_VERSION:+$GOLANG_VERSION-}alpine
LABEL \
  org.opencontainers.image.title="Golang build" \
  org.opencontainers.image.description="Container for building Golang projects; contains golang, dep, golangci-lint and upx"

ENV PS1='[`id -u`:`id -g`@\h $PWD]\$ '
COPY --from=scriptsrc include/common include/golang /
COPY --from=scriptsrc include/install/usr/libexec/RUN.d/github.sh /usr/libexec/RUN.d/

#ENV GOPATH="/go" \
#    GOBIN="/usr/local/bin" \
#    GOPRIVATE="git.wajo.com"

RUN \
  apk --no-cache -v upgrade --available --latest &&\
  apk --no-cache -v add g++ gcc git git-subtree jq make curl ca-certificates tar xz zlib &&\
  update-ca-certificates

# Install UPX
ARG UPX_VERSION=latest
RUN source /usr/libexec/RUN.sh; upx_install

# Install golangci-lint
ARG GOLANGCI_VERSION=latest
RUN source /usr/libexec/RUN.sh; golangci_install
COPY --chown=0:0 include/golangci.yml /etc/golangci.yml

## Enable go mod after last go get in this Dockerfile
ENV GO111MODULE="on"
