ARG FROM_TAG=latest
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Ruby" \
  org.opencontainers.image.description="Container for doing stuff with Ruby"

COPY --from=scriptsrc include/ruby /

#  	g++ ruby-devel make gcc redhat-rpm-config &&\
RUN source /usr/libexec/RUN.sh; pkg_run \
	git-core jq \
	ruby rubygems
