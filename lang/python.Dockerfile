ARG FROM_TAG=latest
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Python Language development" \
  org.opencontainers.image.description="Container with useful Python toolz"

COPY --from=scriptsrc include/python /

RUN source /usr/libexec/RUN.sh; pkg_run \
	diffutils findutils git-core jq make tar \
	python python-pip python-wheel python-psutil python-regex \
	yamllint

RUN source /usr/libexec/RUN.sh; pip_install \
	Cython pycodestyle modernize \
  PyLint saltpylint pylint-protobuf pylint-venv \
  flake8_strict flake8-docstrings flake8-json \
  mock pytest pytest-salt pytest-timeout pytest-tempdir pytest-helpers-namespace \
  && fetch -o /etc/pylintrc https://raw.githubusercontent.com/saltstack/salt-pylint/master/.pylintrc

RUN source /usr/libexec/RUN.sh; py_compile

ENV PIP_NO_CACHE_DIR=1
ENV XDG_CONFIG_HOME=/etc
COPY --chown=0:0 include/yamllint.yml /etc/yamllint/config
